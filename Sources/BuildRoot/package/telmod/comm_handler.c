#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>

#include "comm_handler.h"
#include "cmd_cfg.h"

int ATCommandStatus;




extern at_cmd_struct sim_cmd_table[];


int IssueATCommand(int aCommandId)
{
	int buff_i, result = F_ERRROR, uart_result_init, uart_result_tx, uart_result_rx;
	unsigned char tx_buffer[20];
	unsigned char rx_buffer[256];
	unsigned char *p_tx_buffer;
	at_cmd_struct * at_info;

	ATCommandStatus = CMD_NOT_STARTED;

	/* stream handler for UART */
	int fs_uart0 = UART_STREAM_INVALID;

	uart_result_init = Uart_Init(&fs_uart0, UART_INIT);

	if ((UART_INIT_SUCCESS == uart_result_init) && (AT_LAST > aCommandId))
	{
		at_info = &sim_cmd_table[aCommandId];
		/*
		printf("AT cmd: %s\n", at_info->cmd_string);
		printf("AT cmd_s: %i\n", at_info->cmd_size);
		printf("AT res: %s\n", at_info->response_string);
		printf("AT res_s: %i\n", at_info->response_size);
		*/

		p_tx_buffer = &tx_buffer[0];
		for (buff_i=0; buff_i < (at_info->cmd_size); buff_i++)
		{
			*p_tx_buffer++ = at_info->cmd_string[buff_i];
		}

		uart_result_tx = UartSend(&fs_uart0, &tx_buffer[0], at_info->cmd_size);
		uart_result_rx = UartReceive(&fs_uart0, &rx_buffer[0], MAX_RESPONSE_LEN);


		if ((UART_TX_SUCCESS == uart_result_tx) && (UART_RX_SUCCESS == uart_result_rx))
		{
			result = F_SUCCESS;
		}
		else
		{
			printf("UART TX/RX failed\n");
		}

	}
	else if ((UART_INIT_SUCCESS != uart_result_init))
	{
		printf("UART initialization failed\n");
	}
	else
	{
		printf("Unknown AT command\n");
	}

	(void)Uart_Init(&fs_uart0, UART_DOWN);

	return result;
}

int UartSend(int * UartFS, unsigned char * TxBuff, int TxSize)
{
	int result = UART_TX_ERROR;
	int tx_count;
	unsigned char *p_tx_buff = TxBuff + TxSize;

	tcflush(*UartFS, TCIFLUSH);

	if (UART_STREAM_INVALID != *UartFS)
	{
		*p_tx_buff++ = '\n';
		*p_tx_buff++ = '\r';
		tx_count = write(*UartFS, TxBuff, TxSize+2);
		if (0 > tx_count)
		{
			printf("UART TX error\n");
		}
		else
		{
			result = UART_TX_SUCCESS;
		}
	}
	else
	{
		printf("No TX available\n");
	}

	return result;
}


int UartReceive(int *UartFS, unsigned char * RxBuff, int maxIn)
{
	int result = UART_RX_ERROR;
	int rx_length;

	tcflush(*UartFS, TCIFLUSH);

	if (UART_STREAM_INVALID != *UartFS)
	{
		// Read up to 255 characters from the port if they are there
		do{
			rx_length = read(*UartFS, (void*)RxBuff, maxIn);
		}while (rx_length < 0);

		if (rx_length == 0)
		{
			printf("No data read\n");
		}
		else
		{
			//Bytes received
			RxBuff[rx_length] = '\0';
			printf("%i bytes read : %s\n", rx_length, RxBuff);

			result = UART_RX_SUCCESS;
		}
	}
	else
	{
		printf("No RX available\n");
	}

	return result;
}





int Uart_Init(int *UartFS, uart_init_process UartInitOpt)
{
	int result = UART_INIT_ERROR;
	if (UartInitOpt == UART_INIT)
	{

		/* Open UART */
		*UartFS = open("/dev/ttyAMA0", O_RDWR | O_NOCTTY | O_NDELAY);
		if (UART_STREAM_INVALID == *UartFS)
		{
			//ERROR - CAN'T OPEN SERIAL PORT
			printf("Error - Unable to open UART.  Ensure it is not in use by another application\n");
		}

		/* Configure UART */
		struct termios options;
		tcgetattr(*UartFS, &options);
		options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
		options.c_iflag = IGNPAR;
		options.c_oflag = 0;
		options.c_lflag = 0;

		result = UART_INIT_SUCCESS;
	}
	else
	{
		/* Close UART */
		close(*UartFS);
		result = UART_INIT_SUCCESS;
	}

	return result;
}





/*
rx_state rx_receive(unsigned char num_of_bytes)
{
	 rx_state ret_val = RX_ONGOING;

	 // Rx state machine
	 // ----------------

	 if (rx_state == RX_NOT_STARTED) {
		  // Reception is not started yet - check tmout
		  if (!_cell.available()) {
			   // still no character received => check timeout

			   #ifdef DEBUG_GSMRX

					DebugPrint("\r\nDEBUG: reception timeout", 0);
					Serial.print((unsigned long)(millis() - prev_time));
					DebugPrint("\r\nDEBUG: start_reception_tmout\r\n", 0);
					Serial.print(start_reception_tmout);


			   #endif

			   if ((unsigned long)(millis() - prev_time) >= start_reception_tmout) {
					// timeout elapsed => GSM module didn't start with response
					// so communication is takes as finished

						#ifdef DEBUG_GSMRX
							DebugPrint("\r\nDEBUG: RECEPTION TIMEOUT", 0);
						#endif

					comm_buf[comm_buf_len] = 0x00;
					ret_val = RX_TMOUT_ERR;
			   }
		  } else {
			   // at least one character received => so init inter-character
			   // counting process again and go to the next state
			   prev_time = millis(); // init tmout for inter-character space
			   rx_state = RX_ALREADY_STARTED;
		  }
	 }

	 if (rx_state == RX_ALREADY_STARTED) {
		  // Reception already started
		  // check new received bytes
		  // only in case we have place in the buffer
		  num_of_bytes = _cell.available();
		  // if there are some received bytes postpone the timeout
		  if (num_of_bytes) prev_time = millis();

		  // read all received bytes
		  while (num_of_bytes) {
			   num_of_bytes--;
			   if (comm_buf_len < COMM_BUF_LEN) {
					// we have still place in the GSM internal comm. buffer =>
					// move available bytes from circular buffer
					// to the rx buffer
					*p_comm_buf = _cell.read();

					p_comm_buf++;
					comm_buf_len++;
					comm_buf[comm_buf_len] = 0x00;  // and finish currently received characters
					// so after each character we have
					// valid string finished by the 0x00
			   } else {
					// comm buffer is full, other incoming characters
					// will be discarded
					// but despite of we have no place for other characters
					// we still must to wait until
					// inter-character tmout is reached

					// so just readout character from circular RS232 buffer
					// to find out when communication id finished(no more characters
					// are received in inter-char timeout)
					_cell.read();
			   }
		  }

		  // finally check the inter-character timeout

		  #ifdef DEBUG_GSMRX

				DebugPrint("\r\nDEBUG: intercharacter", 0);
		  <			Serial.print((unsigned long)(millis() - prev_time));
				DebugPrint("\r\nDEBUG: interchar_tmout\r\n", 0);
				Serial.print(interchar_tmout);


		  #endif

		  if ((unsigned long)(millis() - prev_time) >= interchar_tmout) {
			   // timeout between received character was reached
			   // reception is finished
			   // ---------------------------------------------


			   #ifdef DEBUG_GSMRX

				DebugPrint("\r\nDEBUG: OVER INTER TIMEOUT", 0);
			   #endif

			   comm_buf[comm_buf_len] = 0x00;  // for sure finish string again
			   // but it is not necessary
			   ret_val = RX_FINISHED;
		  }
	 }


	 return (ret_val);
}
*/


