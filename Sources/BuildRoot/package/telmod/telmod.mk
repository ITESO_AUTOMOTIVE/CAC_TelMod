TELMOD_VERSION = 0.0.1
TELMOD_SITE = /home/alejandro/Sources/telmod 
TELMOD_SITE_METHOD = local
TELMOD_INSTALL_TARGET = YES

define TELMOD_EXTRACT_CMDS
	cp package/telmod/telmod.c $(@D)/
endef

define TELMOD_BUILD_CMDS
	(cd $(@D); $(TARGET_CC) -Wall -Os -s telmod.c -o telmod)
endef

define TELMOD_INSTALL_TARGET_CMDS
	install -m 0755 -D $(@D)/telmod $(TARGET_DIR)/usr/bin/telmod
endef

$(eval $(generic-package))

