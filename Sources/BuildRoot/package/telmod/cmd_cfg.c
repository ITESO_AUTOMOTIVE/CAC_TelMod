#include "cmd_cfg.h"

at_cmd_struct sim_cmd_table[]=
{
		{"AT",2,"OK",2, 					"Module status acknowledge"},
		{"ATI",3,"SIM808 R14.18",13,		"Module product information"},
		{"AT+CGNSPWR=1",12,"OK",2,			"GPS power on"},
		{"AT+CGNSPWR=0",12,"OK",2,			"GPS power off"},
		{"AT+CGNSINF",10,"",0,				"GPS information"}
};
