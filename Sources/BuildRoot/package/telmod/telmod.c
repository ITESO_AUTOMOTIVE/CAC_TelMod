#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>

#include "comm_handler.h"
#include "cmd_cfg.h"

#define MODULE_SIM808

extern at_cmd_struct sim_cmd_table[];


main()
{
	int opt_menu;
	unsigned char opt_sel[24], start_opt='a';

	printf("ITESO telmod TEST 09!\n");

	printf("Telmod test menu.\nChoose one option:\n\n");

	for(opt_menu=0; opt_menu < AT_LAST; opt_menu++)
	{
		printf("%c - %s (command \"%s\").\n", ((int)start_opt + opt_menu), sim_cmd_table[opt_menu].description, sim_cmd_table[opt_menu].cmd_string);
	}
	printf("\nOption: ");
	scanf("%c", &opt_sel[0]);

	(void)IssueATCommand(((int)opt_sel[0]-(int)start_opt));



}


