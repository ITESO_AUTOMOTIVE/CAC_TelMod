#ifndef COMM_HANDLER_H
#define COMM_HANDLER_H

#define UART_STREAM_INVALID -1

typedef enum{
	F_SUCCESS = 0,
	F_ERRROR
}func_error;

typedef enum{
     RX_NOT_STARTED = 0,     	/* RX not started */
     RX_ONGOING,              	/* RX is ongoing */
     RX_LAST_ITEM,
     RX_TMOUT_ERR,         		/* RX timeout */
	 RX_FINISHED				/* RX finished */
}rx_state;

typedef enum{
	CMD_NOT_STARTED = 0,
	CMD_BUSY,
	CMD_COMPLETED_ERROR,
	CMD_COMPLETED_SUCCESS
}at_cmd_exec;

typedef enum{
    RX_EXPECTED,     			/* RX completed with expected data */
    RX_UNEXPECTED				/* Rx completed with unexpected data*/
}rx_result;


typedef enum{
	UART_INIT_ERROR = 0,
	UART_INIT_SUCCESS,
	UART_TX_ERROR,
	UART_TX_SUCCESS,
	UART_RX_ERROR,
	UART_RX_SUCCESS
}uart_result;

typedef enum{
	UART_INIT,
	UART_DOWN
}uart_init_process;



int IssueATCommand(int aCommandId);


int UartSend(int * UartFS, unsigned char * TxBuff, int TxSize);
int UartReceive(int *UartFS, unsigned char * RxBuff, int maxIn);
int Uart_Init(int *UartFS, uart_init_process UartInitOpt);


#endif
