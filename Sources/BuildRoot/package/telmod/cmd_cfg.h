#ifndef CMD_CFG_H
#define CMD_CFG_H

#define MAX_RESPONSE_LEN 255

typedef struct{
	unsigned char * cmd_string;
	int cmd_size;
	unsigned char * response_string;
	int response_size;
	unsigned char * description;
}at_cmd_struct;

typedef enum{
	AT_ACK,					/* module status check */
	AT_PROD_INFO,			/* product identification information */
	AT_GPS_ON,
	AT_GPS_OFF,
	AT_GPS_INFO,
	AT_LAST
}at_cmd_list;

#endif
