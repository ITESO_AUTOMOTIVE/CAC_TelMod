/*******************************************************************************************/
/*  File:       hmi_shared_memory.h                                                        */
/*  Brief:      HMI definitions to use on hmi_shared_memory                                */
/*  Author:     Raul Camacho/Rafael Cabrera                                                */
/*  Version:    1.0                                                                        */
/*  Date:       09/Oct/2015                                                                */
/*******************************************************************************************/

#ifndef HMI_SHARED_MEMORY_H     /* Prevent duplicated includes */
#define HMI_SHARED_MEMORY_H

/*******************************************************************************************/
/*  TYPES                                                                                  */
/*******************************************************************************************/
typedef struct{
    float 		battery;
    float		env_temp;
    float		speed;
    float		fuel;
    float		efficiency;
    uint32_t 	odometer;
    uint16_t	rpms;
    uint8_t		tire_presure_1;
    uint8_t		low_presure_1_warning;
    uint8_t 	tire_presure_2;
    uint8_t 	low_presure_2_warning;
    uint8_t		tire_presure_3;
    uint8_t		low_presure_3_warning;
    uint8_t 	tire_presure_4;
    uint8_t 	low_presure_4_warning;
    uint8_t 	gear;
    uint8_t 	compass;
    uint8_t 	high_beam;
    uint8_t 	low_beam;
    uint8_t 	optical_horn;
    uint8_t 	turn_right;
    uint8_t 	turn_left;
    uint8_t 	hazard_warning_light;
    uint8_t 	abs_break;
    uint8_t 	airbag;
    uint8_t 	key_status;
    uint8_t 	low_battery;
    uint8_t 	low_fuel;
    uint8_t 	check_engine;
    uint8_t 	seat_belt;
    uint8_t 	hand_brake;
    uint8_t 	sattelital_notification;
    uint8_t 	oil;
    uint8_t 	door_warning_light;
    uint8_t 	motor_temperature_warning;
} tshared_memory;
/*******************************************************************************************/

/*******************************************************************************************/
/*  FUNCTION PROTOTYPES                                                                    */
/*******************************************************************************************/
uint8_t hmi_init_shared_memory(void);
void hmi_deinit_shared_memory(void);
uint8_t hmi_compare_data(tshared_memory* valid_data);
void hmi_copy_data(tshared_memory* valid_data);
/*******************************************************************************************/

#endif      /* HMI_SHARED_MEMORY_H */

/*******************************************************************************************/
